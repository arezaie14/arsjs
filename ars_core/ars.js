function ArsCore() {
    this.boot = function () {
        ars = new ArsCore();
        this.include("./config");
        this.include("./managers/event_manager");
        this.include("./managers/template_manager");
        this.include("./managers/api_manager");
        this.include("./managers/db_manager");
        this.include("./managers/module_manager");
        this.include("./managers/file_manager");
        this.include("./managers/error_manager");

        this.include('./functions/bootstrap');
        return true;
    };

    this.include = function (file_name) {
        delete require.cache[require.resolve(file_name)];
        return require(file_name);
    }
}

ars = new ArsCore();
ars.include("./config");