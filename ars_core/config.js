var path = require('path');

ars.config = {
    db_connection_string: 'mongodb://localhost:27017/arsjs',
    session_key: '$2a$10$ONeFxbr5U9.ZpalQTL2YYeJ0CVAYB8I7I0wi.cwGLNYLyD2pYtZvO',
    address: {
        api: "api/",
        admin: "admin/",
    },
    path: {
        modules: path.join(__dirname, '../modules/'),
        templates: path.join(__dirname, '../templates/'),
    },
    ajax_errors_reason_code: {
        ARS_ABORT_REASON_NORMAL: '0',
        ARS_ABORT_REASON_LOGIN: '1'
    }
};

modules_path = ars.config.path.modules;
templates_path = ars.config.path.templates;