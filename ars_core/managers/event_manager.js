ars.event_manager = {
    events: {},
    on: function (event_name, callback) {
        if (!this.events[event_name]) {
            this.events[event_name] = [];
        }
        this.events[event_name].push(callback);
    },
    trigger: function (event_name,... data) {
        if (this.events[event_name]) {
            this.events[event_name].forEach(fn => fn(... data));
        }
    }
};