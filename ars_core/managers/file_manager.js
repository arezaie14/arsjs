var path = require('path');
var filesystem = require("fs");

ars.file_manger = {
    normalize: function (file_path) {
        return path.normalize(file_path);
    },
    create_file(file_name, data, call_back) {
        filesystem.writeFile(file_name, data, error => {
            if (typeof call_back === "function")
                call_back(error);
        });
    },
    get_file_tree: function (dir, file_name, is_ext = false) {
        var results = [];

        filesystem.readdirSync(dir).forEach(function (file) {
            var is_file = false;

            if (!is_ext && file === file_name)
                is_file = true;
            else if (is_ext && path.extname(file) === file_name)
                is_file = true;

            file = dir + '/' + file;

            var stat = filesystem.statSync(file);

            if (stat && stat.isDirectory())
                results = results.concat(ars.file_manger.get_file_tree(file, file_name));
            else if (is_file)
                results.push(path.normalize(file));
        });

        return results;
    },
    file_exist_sync: function (path) {
        return filesystem.existsSync(path);
    },
    read_file_sync: function (path, mode = "utf-8") {
        return filesystem.readFileSync(path, mode);
    }

};