ars.error_manager = class ArsError {
    status = 0;
    message = "";

    constructor(message, status) {
        this.message = message;
        this.status = status;
    }
};