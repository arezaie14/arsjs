var path = require("path");
var express = require('express');
var crypto = require("crypto");
var HTMLParser = require('node-html-parser');

ars.module_manager = {
    installed_modules: {},
    routes: {},

    run_all_modules(app) {
        var config_files_path = ars.file_manger.get_file_tree(ars.config.path.modules, "config.js");
        for (var i = 0; i < config_files_path.length; i++) {
            const item = config_files_path[i];
            var config = ars.include(item);
            if (config["name"]) {
                if (config["tables"]) {
                    var tables = config["tables"];
                    Object.keys(tables).forEach(function (table_name) {
                        ars.db_manager.add_new_model(table_name, tables[table_name]);
                    });
                    delete config.tables;
                }
                if (config["assets"]) {
                    var assets = config["assets"];
                    Object.keys(assets).forEach(function (asset) {
                        app.use(asset, express.static(path.normalize(assets[asset])));
                    });
                    delete config.assets;
                }
                let boot = path.normalize(item + "/../" + config.name);
                if (file_exist_sync(boot + ".js")) {
                    var construct = ars.include(boot);
                    if (typeof construct.load === "function") {
                        config.load = construct.load;
                    }
                    if (typeof construct.routes !== undefined) {
                        ars.module_manager.routes[config["name"]] = construct.routes;
                    }
                }
                let module_functions = path.normalize(item + "/../functions.js");
                if (file_exist_sync(module_functions)) {
                    ars.include(module_functions);
                }
                ars.module_manager.installed_modules[config["name"]] = config;
                console.log("module: " + config["name"] + " is running.");
            }
        }
        event_trigger("modules_loaded");
    },
    load: function (module_name, template, params) {
        let is_api = false;
        let not_found_err = "Module not found";
        if (typeof module_name === "object") {
            is_api = true;
            if (module_name["module_name"]) {
                if (module_name["template"])
                    template = module_name.template;
                else
                    template = "index";
                if (module_name["params"])
                    params = module_name.params;
                module_name = module_name["module_name"];
            } else {
                return api_return_error(not_found_err);
            }
        }
        if (this.installed_modules[module_name]) {
            var module_base_path = path.normalize(ars.config.path.modules);
            var module_path = path.normalize(module_base_path + "/" + module_name + "/views/" + template);
            if (module_path.indexOf(module_base_path) < 0) {
                if ((is_api))
                    return api_return_error(not_found_err);
                else
                    return not_found_err;
            }
            if (!file_exist_sync(module_path + ".ejs")) {

                if ((is_api))
                    return api_return_error(not_found_err);
                else
                    return not_found_err;
            }

            if (typeof params != "object") {
                params = {};
            }

            if (typeof params["id"] === "undefined") {
                params["id"] = crypto.randomBytes(5).toString("hex");
            }

            var html = ars.module_manager.installed_modules[module_name].load(module_path, params);

            var module_in_html = this.render_modules_in_string(html);
            if (module_in_html)
                html = module_in_html;

            return ("<div ars='module' id='" + params["id"] + "' module='" + module_name + "' template='" + template + "'>" + html + "</div>");
        }
        if (is_api)
            return api_return_error(not_found_err);
        return not_found_err;
    },
    render_modules_in_string: function (str) {
        var res = str.match(/<module(.*?)\/>/g);
        if (res && res.length > 0) {
            for (var i = 0; i < res.length; i++) {
                var module_params = HTMLParser.parse(res[i]).childNodes[0].rawAttrs;
                module_params = module_params.replace(/ /g, '&');
                module_params = module_params.replace(/"/g, '');
                module_params = new URLSearchParams(module_params);
                let params_to_pass = {};
                for (const [key, value] of module_params) {
                    params_to_pass[key] = value;
                }
                if (!params_to_pass["type"])
                    continue;

                module_params = {};
                module_params["module_name"] = params_to_pass["type"];
                if (params_to_pass["template"])
                    module_params["template"] = params_to_pass["template"];

                delete params_to_pass["type"];
                delete params_to_pass["template"];

                module_params["params"] = params_to_pass;

                var module_data = ars.module_manager.load(module_params);
                str = str.replace(res[i], module_data);
            }
            return str;
        }
        return false;
    }
};
