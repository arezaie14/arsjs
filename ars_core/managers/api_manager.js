ars.api_manager = {
    exposed: {},
    expose: function (fn, version = "v1") {
        if (typeof this.exposed[version] == "undefined")
            this.exposed[version] = {};


        if (!this.exposed[version][fn.name]) {
            var args = fn.toString().match(/.*?\(([^)]*)\)/)[1];
            args = args.split(',').map(function (arg) {
                return arg.replace(/\/\*.*\*\//, '').trim();
            }).filter(function (arg) {
                return arg;
            });

            if (args.includes("no_sanitize")) {
                this.exposed[version][fn.name] = fn;
            } else {
                this.exposed[version][fn.name] = function (params, req, res, next) {
                    params = sanitize_obj(params);
                    return fn(params, req, res, next);
                };
            }
        }
    },
    url: function (fn_name, version = "v1") {
        return ars.base_url + this.url_without_server_name(fn_name, version);
    },
    url_without_server_name: function (fn_name, version = "v1") {
        return "api/" + version + "/" + fn_name;
    },
    return_err: function (message) {
        return {error: message}
    },
    call: function (fn, params, req, res, next, version = "v1") {
        if (typeof this.exposed[version] != "undefined") {
            if (typeof this.exposed[version][fn] != "undefined") {
                return this.exposed[version][fn](params, req, res, next);
            }
        }
        return api_return_error("Can't access this API");
    }
};
