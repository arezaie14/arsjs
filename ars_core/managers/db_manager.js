mongoose = require('mongoose');
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

ars.db_manager = {
    connected: false,
    models: {},
    connect: function () {
        mongoose.connect(ars.config.db_connection_string);
        var db = mongoose.connection;

        db.on('error', function () {
            ars.db_manager.connected = false;
            console.error.bind(console, 'connection error:')
        });
        db.once('open', function () {
            ars.db_manager.connected = true;
        });
    },
    add_new_model(table_name, schema) {
        ars.db_manager.models[table_name] = mongoose.model(table_name, schema);
        ars.db_manager.models[table_name].createCollection().then((data) => {
            console.log("Collection: " + table_name + " was installed.")
        }).catch(err => {
            console.log("Error: " + err + " happend while installing collection:" + table_name )
        });
    },
    get_table: function (table_name) {
        return ars.db_manager.models[table_name];
    }
};

ars.db_manager.connect();
