var ejs = require('ejs');

ars.template_manager = {
    render_async: async function (template_path, data, options, ext = ".ejs") {
        return (await ejs.renderFile(ars.file_manger.normalize(template_path + ext), data, options));
    },
    render_sync: function (template_path, data, options, ext = ".ejs") {
        let html = ars.file_manger.read_file_sync(ars.file_manger.normalize(template_path + ext), 'utf-8');
        return ejs.render(html, data, options);
    },
    render_template: function (template_path, data, options) {

        let not_found_err = "Template not found";

        let template_base_path = ars.file_manger.normalize(ars.config.path.templates);
        template_path = ars.file_manger.normalize(template_base_path + "/" + template_path + ".ejs");
        if (template_path.indexOf(template_base_path) < 0) {
            return api_return_error(not_found_err);
        }

        if (!file_exist_sync(template_path )) {
            return api_return_error(not_found_err);
        }

        let html = ars.file_manger.read_file_sync(ars.file_manger.normalize(template_path ), 'utf-8');
        let modules_rendered = ars.module_manager.render_modules_in_string(html);
        if (modules_rendered)
            html = modules_rendered;

        return ejs.render(html, data, options);
    }
};