var sanitize_html = require('sanitize-html');

validate_email = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

sanitize_string = function (str) {
    return sanitize_html(str);
};
sanitize_obj = function (obj) {
    for (const i in obj) {
        let key = sanitize_html(i);
        if (key != i) {
            delete obj[i];
            continue;
        }
        if (typeof obj[i] === "object") {
            obj[i] = sanitize_obj(obj[i]);
        } else {
            obj[i] = sanitize_html(obj[i]);
        }
    }
    return obj;
};