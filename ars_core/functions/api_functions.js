api_expose = function (fn_name, version = "v1") {
    return ars.api_manager.expose(fn_name, version);
};

api_url = function (fn_name, version = "v1") {
    return ars.api_manager.url(fn_name, version);
};

api_url_without_server_name = function (fn_name, version = "v1") {
    return ars.api_manager.url_without_server_name(fn_name, version);
};

api_return_error = function (messaage) {
    return ars.api_manager.return_err(messaage);
};