path_normalize = function (path) {
    return ars.file_manger.normalize(path);
};

get_file_tree = function (dir, file_name, is_ext = false) {
    return ars.file_manger.get_file_tree(dir, file_name, is_ext);
};

create_file = function (file_name, data, call_back) {
    return ars.file_manger.create_file(file_name, data, call_back);
};

file_exist_sync = function (path) {
    return ars.file_manger.file_exist_sync(path);
};

read_file_sync = function (path, mode = "utf-8") {
    return ars.file_manger.read_file_sync(path, mode);
};