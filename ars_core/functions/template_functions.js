view_render_async = function (template_path, data, options, ext = ".ejs") {
    return ars.template_manager.render_async(template_path, data, options, ext);
};

view_render_sync = function (template_path, data, options, ext = ".ejs") {
    return ars.template_manager.render_sync(template_path, data, options, ext);
};

render_template = function (template_path, data, options) {
    return ars.template_manager.render_template(template_path, data, options);
};