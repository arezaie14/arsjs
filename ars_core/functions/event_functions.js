event_on = function (event_name, callback) {
    return ars.event_manager.on(event_name, callback)
};
event_trigger = function (event_name,... data) {
    return ars.event_manager.trigger(event_name,... data)
};