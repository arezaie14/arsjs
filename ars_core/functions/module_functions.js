run_all_modules = function (app) {
    return ars.module_manager.run_all_modules(app);
};

load_module = function (module_name, template, params) {
    return ars.module_manager.load(module_name, template, params);
};
api_expose(load_module);