var router = require('express').Router();

/* Saving request to Ars for future use */
router.all('/:all(*)', function (req, res, next) {
    ars.request = req;
    ars.base_url = req.protocol + '://' + req.get('host') + "/";
    ars.protocol = req.protocol;
    ars.host = req.get('host');
    next();
});

/* mounting modules routes */
var modules_name = Object.keys(ars.module_manager.routes);
for (let module_idx in modules_name) {
    let methods = ars.module_manager.routes[modules_name[module_idx]];
    for (let method in methods) {
        for (let route_path in methods[method]) {

            var fn = methods[method][route_path];
            var args = fn.toString().match(/.*?\(([^)]*)\)/)[1];
            args = args.split(',').map(function (arg) {
                return arg.replace(/\/\*.*\*\//, '').trim();
            }).filter(function (arg) {
                return arg;
            });
            if (args.includes("no_sanitize")) {
                router[method](route_path, function (req, res, next) {
                    var fn = methods[method][route_path];
                    fn(req, res, next);
                });
            } else {
                router[method](route_path, function (req, res, next) {
                    if (req.method === 'GET' || req.method === 'COPY' || req.method === 'PURGE' || req.method === 'UNLOCK')
                        req.query = sanitize_obj(req.query);
                    else
                        req.body = sanitize_obj(req.body);
                    var fn = methods[method][route_path];
                    fn(req, res, next);
                });
            }
        }
    }
}

/* Api */
router.all('/api/:version/:fn_name', function (req, res, next) {
    var data = {};
    if (req.method === 'GET' || req.method === 'COPY' || req.method === 'PURGE' || req.method === 'UNLOCK')
        data = req.query;
    else
        data = req.body;

    let fn = ars.api_manager.call(req.params.fn_name, data, req, res, next, req.params.version);
    if (fn.constructor.name === "ArsError") {
        res.set('ars_abort', 'true');
        res.status(fn.status);
        return res.send(fn.message);
    }
    if (fn.constructor.name !== "Promise") {
        return res.send(fn);
    }
    fn.then(htm => {
        if (htm.constructor.name === "ArsError") {
            res.set('ars_abort', 'true');
            res.status(htm.status);
            return res.send(htm.message);
        }
        return res.send(htm);
    });

});

/* To reboot Ars To install modules */
router.all('/ars_reboot', function (req, res, next) {
    ars.boot();
    res.send(req.params.all);
});

/* To all routes */
router.all('/:all(*)', function (req, res, next) {
    res.status(404).send("Page not found!");
});

module.exports = router;