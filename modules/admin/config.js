module.exports = {
    name: "admin",
    ui: false,
    ui_admin: true,
    assets: {
        "/admin": path_normalize(ars.config.path.templates + "/admin/assets")
    },
};