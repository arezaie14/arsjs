let axios = require('axios');

send_sms_to_phone = async (params) => {
    if (!params["phone"] || !params["message"])
        return api_return_error(_e("Error SM1, Please call administrator!"));

    return await new Promise(resolve => {
        axios.post('https://api.sabanovin.com/v1/' + ars.module_manager.installed_modules.short_message.api_key + '/sms/send.json', {
            'gateway': ars.module_manager.installed_modules.short_message.line_number,
            'to': params["phone"],
            'text': params["message"],
        }).then((res) => {
            res = res.data;
            if (!res["status"])
                return resolve(api_return_error(_e("Error SM2, Please call administrator")));
            if (!res.status["code"])
                return resolve(api_return_error(_e("Error SM3, Please call administrator")));

            if (res.status.code === 200) {

                new (table("short_messages"))({
                    from: ars.module_manager.installed_modules.short_message.line_number,
                    to: params["phone"],
                    message: params["message"]
                }).save();

                return resolve(res);
            } else
                return resolve(api_return_error(_e("Error: " + res.status.code + " happened while sending message!")));


        }).catch((error) => {
            console.error(error)
        });
    });
};

api_expose(send_sms_to_phone);