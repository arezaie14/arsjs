module.exports = {
    name: "short_message",
    api_key: "sa567029979:cS7XuMGD3hrITDhGApUOYYEKUIH3CzNYJYTP",
    line_number: "200020003000",
    tables: {
        short_messages: {
            from: String,
            to: String,
            message: String,
            user_id: String,
            created_at: {type: Date, default: Date.now()}
        }
    }
};