var minify = require('minify');

module.exports = {
    routes: {
        get: {
            "/reset_asset_cache": function (req, res, next) {
                minify_files(__dirname + "/js", "js", __dirname + "/cache/ars.js", function () {
                    minify_files(__dirname + "/css", "css", __dirname + "/cache/ars.css", function () {
                        return res.send("done");
                    });
                });
            }
        }
    }
};

function minify_files(path, ext, ex_file_path, call_back) {

    let js_files = get_file_tree(path, ("." + ext), true);
    let data_to_compress = "";
    let idx_sum = 0;
    let done_idx = 0;
    js_files.forEach(function (file_name, index) {
        view_render_async(file_name, {}, {}, "").then(function (data) {
            data_to_compress += data;
            done_idx += index;
            if (done_idx >= idx_sum) {
                create_file(path_normalize(ex_file_path), minify[ext](data_to_compress));
                if (typeof call_back === "function")
                    call_back();

            }
        });
        idx_sum += index;
    });
}