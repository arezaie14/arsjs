var ars = {};
ars.load_module_api = window.location.protocol + "//" + window.location.hostname + ((location.port > 0) ? ":" + location.port : "") + "/<?=api_url_without_server_name('load_module')?>";

ars.api_url = function (api) {
    var api_add = "<?=api_url_without_server_name('{API}')?>";
    api_add = api_add.replace("{API}", api);
    return window.location.protocol + "//" + window.location.hostname + ((location.port > 0) ? ":" + location.port : "") + "/" + api_add;
};
ars.active_module_id = null;

function set_items_to_ele() {
    $(function () {
        $("div[ars='module']").each(function (index, ele) {
            $(ele).mouseover(function (e) {
                e.stopPropagation();
                ars.active_module_id = $(this).attr("id");
            });
        });

        $(".ars-auto-save").each(function (index, ele) {
            $(ele).change(function () {
                data = {};

                data.key = $(ele).attr("name");
                if (data.key == "" || data.key == null || typeof data.key === "undefined")
                    return;
                data.value = $(ele).val();

                if ($(ele).is(':checkbox')) {
                    if ($(ele).is(':checked')) {
                        data.value = $(ele).val();
                    } else {
                        data.value = 0;
                    }
                }

                if ($(ele).attr("group") !== "" && typeof $(ele).attr("group") !== "undefined") {
                    data.group = $(ele).attr("group");
                }
                $.post("<?=api_url('set_option');?>", data, function (rec_data) {
                    if (rec_data !== "false") {
                        ars.notif.success("<?=_e('Saved success')?>");
                    } else {
                        ars.notif.error("<?=_e('Error in saving')?>");
                    }
                });
            });
        })
    });
}

ars.add_active_class_when_module_loaded = function (selector) {
    $(selector).each(function (index, ele) {
        if (!("ars_selector" in $(ele)[0])) {
            $(ele)[0].ars_selector = selector;

            $(ele).click(function () {
                ars.active_Class_selector = selector;
                ars.event.on("module_load", function () {
                    if ($(ele)[0].ars_selector === ars.active_Class_selector) {
                        $(selector).removeClass("active");
                        $(ele).addClass("active");
                    }
                });
            });
        }
    });
};
ars.reload_this = function (data, active_module_id, call_back) {
    var selector = "#" + ars.active_module_id;
    if (active_module_id !== undefined && active_module_id.length > 0)
        selector = "#" + active_module_id;
    var module_name = $(selector).attr("module-name");
    ars.load_module(module_name, selector, call_back, data, true);
};

ars.load_parents_module = function (module_name, data, selector, call_back) {

    if (module_name != null)
        module_name = $("#" + ars.active_module_id).attr("parent-module") + "/" + module_name;
    else
        module_name = $("#" + ars.active_module_id).attr("module-name");

    var obj = {};
    obj.module_name = module_name;
    obj.params = data;
    if (typeof data === "object") {
        if (!(data && "no_pace" in data && data.no_pace === true))
            Pace.restart();
    } else {
        Pace.restart();
    }
    ars.post(ars.load_module_api, obj, function (rec_data) {
        if (selector != null && selector.length > 0) {
            $(selector).html(rec_data);
        } else
            $("#" + ars.active_module_id).replaceWith(rec_data);
        ars.event.trigger("module_load", rec_data);
        set_items_to_ele();
        if (typeof call_back === "function") {
            call_back(rec_data);
        }
    });
};

ars.load_module = function (module_name, selector, call_back, data, replace) {
    var obj = {};
    obj.module_name = module_name;
    obj.params = data;
    if (typeof data === "object") {
        if (!(data && "no_pace" in data && data.no_pace === true))
            Pace.restart();
    } else {
        Pace.restart();
    }
    ars.post(ars.load_module_api, obj, function (rec_data) {
        if ($(selector).length > 0 && replace === true)
            $(selector).replaceWith(rec_data);
        else
            $(selector).html(rec_data);
        ars.event.trigger("module_load", rec_data);
        set_items_to_ele();
        if (typeof call_back === "function") {
            call_back(rec_data);
        }
    });
};

ars.post = function (address, params, callback) {
    ars_dialog.show_loading();
    var ars_post = $.ajax({
        url: address,
        method: "post",
        data: params,
        success: function (data, status, xhr) {
            ars_dialog.close_loading();
            if (typeof callback === "function") {
                callback(data);
            }
        },
        error: function (jqXHR, textStatus, errorMessage) { // error callback
            ars_dialog.close_loading();
            if (jqXHR["responseJSON"] && jqXHR.getResponseHeader("ars_abort")) {
                var data = jqXHR.responseJSON;
                if ("reason_code" in data) {
                    switch (jqXHR.status) {
                        case 403:
                            switch (data["reason_code"]) {
                                case '<?=ars.config.ajax_errors_reason_code.ARS_ABORT_REASON_LOGIN?>':
                                    ars_dialog.show_login();
                                    break;
                            }
                            break;
                        default:
                            ars_dialog.error(data.message);
                            break;
                    }
                }
            } else {
                ars_dialog.error(jqXHR["responseText"]);
            }
        }

    });
};

ars.notif = {
    success: function (message, time) {
        this.show(message, "#008d4c", time);
    },
    error: function (message, time) {
        this.show(message, "#d33724", time);
    },
    warning: function (message, time) {
        this.show(message, "#ff7701", time);
    },
    show: function (message, color, time) {
        if (time === null || time === "" || time <= 0 || time === undefined) time = 1000;

        var container = document.createElement("div");
        var message_box = document.createElement("div");
        $(container).css({display: "flex", justifyContent: "center"});
        $(message_box).css({display: "none", position: "fixed", zIndex: 999999, backgroundColor: color, color: "#ffffff", padding: "10px"});
        $(message_box).append(message);
        $(container).append(message_box);
        $("body").prepend(container);
        $(message_box).fadeToggle("slow");
        setTimeout(function () {
            $(message_box).fadeToggle("fast");
        }, time);

    }
};

ars.persiandp = {
    date_field_jalali_show_to_georgian_save: function (ele) {
        var name = $(ele).attr("name");
        var id = $(ele).attr("id") ? $(ele).attr("id") : "";
        var disabled = $(ele).attr("disabled") ? $(ele).attr("disabled") : "";
        if (id.length > 0) {
            id = "id='g-" + id + "'";
        }
        $(ele).attr("name", "");
        $(ele).addClass("cursor-pointer");
        $(ele).attr("readonly", true);
        var input_to_show_date = $("<input type='hidden' " + id + " " + disabled + " name='" + name + "'/>").insertAfter($(ele));

        $(ele).pDatepicker({
            format: "YYYY-MM-DD",
            autoclose: true,
            formatter: function (unix) {
                var g_date = new persianDate(unix).toCalendar('gregorian').toLocale('en').format("YYYY-MM-DD");
                $(input_to_show_date).val(g_date);
                $(ele).attr("unix_date", unix);
                $(ele).attr("g_date", g_date);
                return new persianDate(unix).toCalendar('persian').format("YYYY-MM-DD")
            }
        })
    }
};
ars.event = {
    fns: {},
    ids: {},
    on: function (name, callback, id) {
        if (!(id !== null && this.ids[id])) {

            if (id != null)
                this.ids[id] = true;

            if (!this.fns[name]) {
                this.fns[name] = [];
            }
            if (typeof callback === "function") {
                this.fns[name].push(callback);
            }
        }
    },
    trigger: function (name, arguments) {
        if (this.fns[name]) {
            $(this.fns[name]).each(function (index, item) {
                item(arguments);
            })
        }
    }
};

ars.cookie = {
    set: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

    },
    get: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    }
}

//run some functions
set_items_to_ele();