$.fn.extend({
    ars_mask: function () {
        var ele = $(this);
        return {
            mask: function () {
                var return_val = true;
                var ars_frm = $(ele);
                var input_to_validate = $("[ars-mask]");
                $(input_to_validate).each(function () {
                    var a = "$(this).ars_mask()." + $(this).attr('ars-mask') + "()";
                    eval(a)
                });
            },
            ars_ir_mobile: function () {
                var element = $(ele);
                $(element).attr("type", "hidden");
                var ele_id = $(element).attr("id");
                var ele_class = $(element).attr("class");
                var ele_val = $(element).val();
                $(element).after("<input type='text' id='" + ele_id + "' class='" + ele_class + "' value='" + ele_val + "'/>");
                var new_ele = $(element).next()[0];
                $(new_ele).inputmask({
                    mask: "0\\999 999 9999",
                    placeholder: '____ ___ ____',
                });
                $(new_ele).keyup(function () {
                    $(element).removeAttr("id");
                    $(element).removeAttr("class");
                    $(element).val($(new_ele).val().replace(/ /g, ""));
                    $(element).val($(element).val().replace(/-/g, ""));
                });
            },
            ars_ir_phone: function () {
                var element = $(ele);
                $(element).inputmask({
                    mask: "(999) 9999 9999",
                    removeMaskOnSubmit: true
                });
            },
            ars_meli_code: function () {
                var element = $(ele);
                $(element).inputmask({
                    mask: "999-999999-9",
                    removeMaskOnSubmit: true
                });
            },
            ars_mail: function () {
                var element = $(ele);
                $(element).inputmask({
                    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
                    greedy: false,
                    onBeforePaste: function (pastedValue, opts) {
                        pastedValue = pastedValue.toLowerCase();
                        return pastedValue.replace("mailto:", "");
                    },
                    definitions: {
                        '*': {
                            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                            casing: "lower"
                        }
                    }
                });
            },
            ars_hour_minute: function () {
                var element = $(ele);
                $(element).inputmask({
                    mask: "*{1,}:~{1}*{1}",
                    greedy: false,
                    onBeforePaste: function (pastedValue, opts) {
                        pastedValue = pastedValue.toLowerCase();
                        return pastedValue.replace("Time:", "");
                    },
                    definitions: {
                        '*': {
                            validator: function (chrs) {
                                return new RegExp("[0-9]").test(chrs);
                            },
                            casing: "lower"
                        },
                        '~': {
                            validator: function (chrs) {
                                return new RegExp("[0-5]").test(chrs);
                            },
                        }
                    }
                });
            },
            ars_num: function () {
                var can_check = true;
                $(ele).val($(ele).val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                var ele_name = $(ele).attr("name");
                $(ele).removeAttr("name");
                var main_number = "<input type='hidden' name='" + ele_name + "'>";
                main_number = $(main_number).insertAfter($(ele));

                $(main_number).val($(ele).val().replace(/,/g, ''));

                $(ele).on("keydown keyup mousedown mouseup", function () {
                    pastValue = this.value;
                    withou_comma = pastValue = pastValue.replace(/,/g, '');
                    $(main_number).val(withou_comma);
                    pastSelectionStart = this.selectionStart;
                    pastSelectionEnd = this.selectionEnd;
                    can_check = true;
                }).on("input", function () {
                    if (can_check) {
                        var regex = /^[0-9]+\.?[0-9]*$/;
                        withou_comma = this.value = this.value.replace(/,/g, '');
                        $(main_number).val(this.value);
                        if (this.value.length > 0 && !regex.test(this.value)) {
                            this.value = pastValue;
                            this.selectionStart = pastSelectionStart;
                            this.selectionEnd = pastSelectionEnd;
                            $(main_number).val(withou_comma);
                            num = this.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            this.value = num;
                        } else {
                            $(main_number).val(withou_comma);
                            num = this.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            this.value = num;
                        }
                        can_check = false;
                    }
                });
            },
        }
    },
});
