var ars_dialog = {
    dialog: swal,
    alert: function (message) {
        this.dialog(message);
    },
    success: function (title, message) {
        if (title == null) title = message;
        if (message == null) message = "";
        this.dialog(title, message, "success")
    },
    error: function (title, message) {
        if (title == null) title = message;
        if (message == null) message = "";
        this.dialog(title, message, "error")
    },
    warning: function (title, message) {
        if (title == null) title = message;
        if (message == null) message = "";
        this.dialog(title, message, "warning")
    },
    confirm: function (message, type, confirm_callback, cancel_callback, confirm_title, cancel_title, show_cancel) {
        if (cancel_title == null || !(cancel_title.length > 0))
            cancel_title = "<?=_e('No')?>";
        if (confirm_title == null || !(confirm_title.length > 0))
            confirm_title = "<?=_e('Yes')?>";
        if (show_cancel !== false)
            show_cancel = true;
        this.dialog({
            title: message,
            type: type,
            showCancelButton: show_cancel,
            cancelButtonText: cancel_title,
            confirmButtonText: confirm_title
        }).then(function (result) {
            if (result.value) {
                if (typeof confirm_callback === "function") {
                    confirm_callback();
                }
            } else {
                if (typeof cancel_callback === "function") {
                    cancel_callback();
                }
            }
        });
    },
    show_loading: function (title) {
        if (title == null) title = "<?=_e('Loading ...')?>";
        this.dialog({
            title: title,
            onOpen: function () {
                ars_dialog.dialog.showLoading()
            }
        })

    },
    close_loading: function () {
        this.dialog.close();
    },
    show_login_callback: null
    , show_login: function () {
        ars_dialog.get_code = function (e, ele) {
            e.preventDefault();
            var obj = $(ele).serialize();
            var phone_number = $(ele).find('input[name="phone_number"]').val();

            ars.post(ars.api_url("send_login_code"), obj, function (rec_data) {
                if (rec_data === true) {
                    ars_dialog.dialog({
                        title: '',
                        showConfirmButton: false,
                        html:
                            "" +
                            "<form onsubmit='ars_dialog.do_login(event,this)'>" +
                            "<div class='form-group'>" +
                            "<label><?=_e('Phone number')?></label>" +
                            "<input type='text' style='text-align:left;' class='form-control' readonly name='phone_number' value='" + phone_number + "'>" +
                            "</div>" +
                            "<div class='form-group'>" +
                            "<label><?=_e('One time password')?></label>" +
                            "<input type='password' style='text-align:left;' class='form-control' name='login_code'>" +
                            "</div>" +
                            "<div class='form-group'>" +
                            "<button type='submit' class='btn btn-success'><?=_e('Login')?></button>" +
                            "</div>" +
                            "</form>"
                    });
                } else if (rec_data["error"] !== undefined) {
                    ars_dialog.error(rec_data["error"]);
                }
            });
        };
        ars_dialog.do_login = function (e, ele) {
            e.preventDefault();
            var obj = $(ele).serialize();
            ars.post(ars.api_url("vardane_login"), obj, function (rec_data) {
                if (rec_data["error"] === "expired") {
                    ars_dialog.confirm('<?=_e("One time password has expired")?>', "warning", function () {
                        ars_dialog.show_login();
                    }, null, '<?=_e("Get new one time password")?>', null, false);
                } else if (rec_data["error"] !== undefined) {
                    ars_dialog.error(rec_data["error"]);
                } else if (rec_data === true) {
                    ars_dialog.close_loading();
                    if (typeof ars_dialog.show_login_callback === "function") {
                        ars_dialog.show_login_callback();
                    }
                }
            });
        };
        ars_dialog.dialog({
            title: '<?=_e("Please get one time password to continue")?>',
            showConfirmButton: false,
            html:
                "" +
                "<form onsubmit='ars_dialog.get_code(event,this)'>" +
                "<div class='form-group'>" +
                "<input type='text' style='text-align:left;' class='form-control' name='phone_number' placeholder=\"<?=_e('Phone number')?>\">" +
                "</div>" +
                "<div class='form-group'>" +
                "<button type='submit' class='btn btn-success'><?=_e('Get one time password')?></button>" +
                "</div>" +
                "</form>"
        });

    }

};