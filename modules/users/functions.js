var Geetest = require('gt3-sdk');
var bcrypt = require('bcryptjs');


passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    table("users").findById(id, function (err, user) {
        if (!err)
            done(null, user);
        else
            done(err, null);
    });
});

passport.use(new LocalStrategy(
    {
        usernameField: 'username',
        passwordField: 'password'
    },
    function (username, password, done) {
        table("users").findOne().or([{"email.address": username}, {"phone.number": username}]).then((user) => {

            if (!user || !bcrypt.compareSync(password, user.password)) {
                return done(api_return_error(_e("Username or password is not correct!")));
            }
            return done(null, user, api_return_error(_e("User ")));
        }).catch((err) => {
            return done(api_return_error(_e("Error UL1 happened, please call administration!")));
        });
    }
));

async function user_register(params) {
    if (typeof params !== "object")
        return api_return_error("Data is incorrect");

    if (!params["email"] || !params["password"])
        return api_return_error(_e("Please check fields!"));

    let search_data = [];

    // params["username"] = params["username"].toLowerCase().trim();
    // search_data.push({"username": params["username"]});


    if (params["email"]) {
        params["email"] = params["email"].toLowerCase().trim();
        if (!validate_email(params["email"]))
            return api_return_error(_e("Please enter valid email address !"));
        search_data.push({"email.address": params["email"]});
    }

    if (params["phone"]) {
        if (params.phone.length > 11)
            return api_return_error(_e("Phone number length is not correct"));
        if (isNaN(params.phone))
            return api_return_error(_e("Please cehck phone number."));

        params["phone"] = params["phone"].toLowerCase().trim();
        search_data.push({"phone.number": params["phone"]});
    }

    return await new Promise(resolve => {
        table("users").findOne().or(search_data)
            .then(user => {
                let registered_data = {
                    // username: params.username,
                    password: bcrypt.hashSync(params["password"], bcrypt.genSaltSync(10)),
                    registered_date: Date.now(),
                    username: params.email,
                };
                if (user) {
                    // if (user.username == params["username"])
                    //     return resolve(api_return_error(_e("This username has been taken, Please select another one !")));

                    if (params["email"] && user["email"] && params["email"] == user.email.address)
                        return resolve(api_return_error(_e("This email has been taken, Please select another one !")));

                    if (params["phone"] && user["phone"] && params["phone"] == user.phone.number)
                        return resolve(api_return_error(_e("This phone number has been taken, Please select another one !")));
                }

                // if (params["email"]) {
                registered_data.email = {
                    address: params.email,
                };
                // }
                if (params["phone"]) {
                    registered_data.phone = {
                        number: params.phone,
                    };
                }

                table("users").create(registered_data, function (err, data) {
                    if (err)
                        return resolve(api_return_error(_e("Error U2 happened, Please call administration! ")));
                    return resolve({success: true, message: _e("signup successful")});
                });
            })
            .catch(error => {
                resolve(api_return_error(_e("Error U1 happened, Please call administration !")));
            });
    });
}

api_expose(user_register);

async function user_login(params, req, res, next) {
    return await new Promise(resolve => {
        if (req.isAuthenticated())
            return resolve({success: false, logged_in: true, error: _e("You are logged in!")});
        passport.authenticate("local", function (err, user, info) {
            if (err)
                return resolve(err);
            req.logIn(user, function (err) {
                if (err) {
                    return resolve(err);
                }
                return resolve({success: true, logged_in: true});
            });

        })(req, res, next);
    });
}

api_expose(user_login);

current_user = function () {
    if (!ars.request || !ars.request.isAuthenticated())
        return false;
    return ars.request.user;
};
is_admin = function () {
    if (!current_user())
        return false;
    return current_user().is_admin;
};
uac = function (ac_name) {
    if (!current_user())
        return false;
    let uacl = current_user().roles;

    return ((ac_name in uacl) ? (uacl[ac_name] === true || uacl[ac_name] === "true" || uacl[ac_name] === 1) : false);
};