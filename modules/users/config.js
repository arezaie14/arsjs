module.exports = {
    name: "users",
    ui: false,
    ui_admin: true,
    tables: {
        users: {
            username: {type: String, unique: true},
            email: {
                address: {type: String, required: true},
                is_active: {
                    type: Boolean,
                    default: false
                },
                is_required_for_login: {type: Boolean, default: false}
            },
            phone: {
                number: String,
                is_active: {
                    type: Boolean,
                    default: false
                },
                is_required_for_login: {type: Boolean, default: false}
            },
            password: {type: String, required: true},

            name: String,
            last_name: String,
            middle_name: String,

            roles: mongoose.Schema.Types.Mixed,

            is_admin: {type: Boolean, default: false},
            is_active: {type: Boolean, default: true},
            registered_date: {type: Date, required: true},
            last_logged_in_date: Date,
            last_used_time: Date,
            reset_password_code: {type: String, default: ""}
        }
    }
};