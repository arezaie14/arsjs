var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var ejs = require('ejs');
passport = require('passport');
LocalStrategy = require('passport-local').Strategy;

var expressSession = require('express-session');
require('./ars_core/ars');


var app = express();

app.use(expressSession({
        secret: ars.config.session_key,
        resave: true,
        saveUninitialized: true,
    })
);

app.use(passport.initialize());
app.use(passport.session());

ars.boot();
run_all_modules(app);

var indexRouter = require('./ars_core/routes');

// view engine setup
app.set('views', path.join(__dirname, 'templates'));
ejs.delimiter = '?';
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

// start routing
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

ars.app = app;

module.exports = app;
